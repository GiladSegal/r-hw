#Lenear model
student <- read.csv("student-mat.csv", sep=";")

str(student)

any(is.na(student)) #empty data?
nrow(student) #number of rows

#Split into trining set and test set
install.packages('caTools')
library(caTools)

#sample <- sample.split()
#split and test in manual way
set.seed = 101
rows <- nrow(student)
split <- runif(rows)
split <- split >=0.3

student.train <- student[split,]
nrow(student.train)

student.test <- student[!split,]
nrow(student.test)
