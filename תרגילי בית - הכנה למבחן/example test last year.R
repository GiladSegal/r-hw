#Example test - last year
str(mtcars)
#mpg gas galon - How economical

#-------------------------------------------------------------------
#Q1 - convert the column of mgp to factor, when yes is bigger than median
median <- median(mtcars$mpg)
con_median <- function(x){
  if (x>median){
    return('Yes')
  } else{
    return('No')
  }
}

mgp_factor <- sapply(mtcars$mpg, con_median)
mtcars$mpg <- mgp_factor
mtcars$mpg <- as.factor(mtcars$mpg)
str(mtcars)
summary(mtcars$mpg)

#-------------------------------------------------------------------
#Q3 - Decision tree
library(caTools)
any(is.na(mtcars)) #False
split <- sample.split(mtcars, 0.7)
train <- subset(mtcars, split==T)
test <- subset(mtcars, split==F)

#Run model
library(rpart)
tree <- rpart(mpg ~ ., method = 'class', data = mtcars)
#Run plot
library(rpart.plot)
prp(tree)

#-------------------------------------------------------------------
#Q4 - Kmeans
cluster <- kmeans(mtcars[, 2:11], 2, nstart = 20)
cluster$cluster
table(cluster$cluster, mtcars$mpg)

#-------------------------------------------------------------------
#Q5 - convert column to category type
str(mtcars)

#Convert cyl
unique(mtcars$cyl) #See the distict values in column
# 6 4 8
catCyl <- cut(mtcars$cyl, breaks = c(0,4,6,8), labels = c('min','mid','max'))

#Convert gear
unique(mtcars$gear) #See the distict values in column
# 4 3 5
catGear <- cut(mtcars$gear, breaks = c(0,3,4,5), labels = c('3Gears','4Gears','5Gears'))







